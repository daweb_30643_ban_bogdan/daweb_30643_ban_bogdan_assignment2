import React from 'react';
import './news.css'
import xmldata from './xml-data/noutati.xml';
import xmldata_en from './xml-data/news.xml';
import axios from "axios";

class NewsContainer extends React.Component {
    constructor(props) {
        super(props);

        this.state = {
            date1:'',
            date2:'',
            date3:'',
            title1:'',
            title2:'',
            title3:'',
            content1:'',
            content2:'',
            content3:''
        }
    }

    componentDidMount() {
        let XMLParser = require('react-xml-parser');
        // let xml = new XMLParser().parseFromString("<Hello>Hello world</Hello>");
        // console.log(xml);
        // console.log(xml.getElementsByTagName('Name'));

        let xml_aux;

        let lang = localStorage.getItem('language')
        if(lang === 'ro') {
            axios.get(xmldata, {
                "Content-Type": "application/xml; charset=utf-8"
            })
                .then((response) => {
                    console.log('Your xml file as string', response.data);
                    //this.setState({xml_data: response.data});
                    xml_aux = new XMLParser().parseFromString(response.data);
                    this.setState({date1: xml_aux.getElementsByTagName('new')[0].getElementsByTagName('date')[0].value})
                    this.setState({date2: xml_aux.getElementsByTagName('new')[1].getElementsByTagName('date')[0].value})
                    this.setState({date3: xml_aux.getElementsByTagName('new')[2].getElementsByTagName('date')[0].value})
                    this.setState({title1: xml_aux.getElementsByTagName('new')[0].getElementsByTagName('title')[0].value})
                    this.setState({title2: xml_aux.getElementsByTagName('new')[1].getElementsByTagName('title')[0].value})
                    this.setState({title3: xml_aux.getElementsByTagName('new')[2].getElementsByTagName('title')[0].value})
                    this.setState({content1: xml_aux.getElementsByTagName('new')[0].getElementsByTagName('content')[0].value})
                    this.setState({content2: xml_aux.getElementsByTagName('new')[1].getElementsByTagName('content')[0].value})
                    this.setState({content3: xml_aux.getElementsByTagName('new')[2].getElementsByTagName('content')[0].value})
                });
        }else{
            axios.get(xmldata_en, {
                "Content-Type": "application/xml; charset=utf-8"
            })
                .then((response) => {
                    console.log('Your xml file as string', response.data);
                    //this.setState({xml_data: response.data});
                    xml_aux = new XMLParser().parseFromString(response.data);
                    this.setState({date1: xml_aux.getElementsByTagName('new')[0].getElementsByTagName('date')[0].value})
                    this.setState({date2: xml_aux.getElementsByTagName('new')[1].getElementsByTagName('date')[0].value})
                    this.setState({date3: xml_aux.getElementsByTagName('new')[2].getElementsByTagName('date')[0].value})
                    this.setState({title1: xml_aux.getElementsByTagName('new')[0].getElementsByTagName('title')[0].value})
                    this.setState({title2: xml_aux.getElementsByTagName('new')[1].getElementsByTagName('title')[0].value})
                    this.setState({title3: xml_aux.getElementsByTagName('new')[2].getElementsByTagName('title')[0].value})
                    this.setState({content1: xml_aux.getElementsByTagName('new')[0].getElementsByTagName('content')[0].value})
                    this.setState({content2: xml_aux.getElementsByTagName('new')[1].getElementsByTagName('content')[0].value})
                    this.setState({content3: xml_aux.getElementsByTagName('new')[2].getElementsByTagName('content')[0].value})
                });
        }
    }

    render(){
        let lang = localStorage.getItem('language')
        return(
                    <div className="page-content">
                        <div className="container">
                            <div className="row">
                                <div className="col-md-12">
                                    <div className="blog-classic">
                                        <div className="date">
                                            {this.state.date1}
                                        </div>
                                        <div className="blog-post">
                                            <h4 className="text-uppercase">
                                                <a>{this.state.title1}
                                                </a></h4>

                                            <p>{this.state.content1}</p>
                                        </div>
                                    </div>
                                    <br/>
                                    <div className="blog-classic">
                                        <div className="date">
                                            {this.state.date2}
                                        </div>
                                        <div className="blog-post">

                                            <h4 className="text-uppercase"><a>{this.state.title2}</a>
                                            </h4>

                                            <p>{this.state.content2}</p>
                                        </div>
                                    </div>
                                    <br/>
                                    <div className="blog-classic">
                                        <div className="date">
                                            {this.state.date3}
                                        </div>
                                        <div className="blog-post">
                                            <h4 className="text-uppercase"><a>{this.state.title3}
                                            </a></h4>
                                            <p>{this.state.content3}</p>
                                        </div>
                                    </div>
                                    <br/>
                                </div>

                            </div>
                        </div>
                    </div>
        );
    }
}

export default NewsContainer;
