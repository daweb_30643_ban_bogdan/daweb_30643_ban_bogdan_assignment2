import React from 'react';
import './navigBar.css';

let lang  = localStorage.getItem('language')

const NavigBar = () => (
    <>
            {lang === 'ro'?
    <div className="topnav">
        <a className="active" href="/">Acasa</a>
        <a href="/news">Noutati</a>
        <a href="/about">Despre noi</a>
        <a href="/medici">Medici</a>
        <a href="/tarife">Servicii & tarife</a>
        <a href="/contact">Contact</a>
    </div>:
    <div className="topnav">
        <a className="active" href="/">Acasa</a>
        <a href="/news">News</a>
        <a href="/about">About us</a>
        <a href="/medici">Doctors</a>
        <a href="/tarife">Services & prices</a>
        <a href="/contact">Contact</a>
    </div>}
    </>
)
export default NavigBar;