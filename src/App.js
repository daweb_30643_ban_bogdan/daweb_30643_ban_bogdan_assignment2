import React from 'react'
import {BrowserRouter as Router, Route, Switch} from 'react-router-dom'
import HomeContainer from "./home/home-container";
import NavigBar from "./navigBar";
import ContactContainer from "./contact/contact-container";
import DoctorsContainer from "./doctors/doctors-container";
import AboutContainer from "./about/about-container";
import TarifeContainer from "./tarife/tarife-container";
import NewsContainer from "./news/news-container";


class App extends React.Component {

  render() {
    return (
        <div>
            <Router>
                <div>
                    <NavigBar />
                    <Switch>
                        <Route
                            exact
                            path={'/'}
                            component={HomeContainer}
                        />
                        <Route
                            exact
                            path={'/news'}
                            component={NewsContainer}
                        />
                        <Route
                            exact
                            path={'/tarife'}
                            component={TarifeContainer}
                        />
                        <Route
                            exact
                            path={'/about'}
                            component={AboutContainer}
                        />
                        <Route
                            exact
                            path={'/medici'}
                            component={DoctorsContainer}
                        />
                        <Route
                            exact
                            path={'/contact'}
                            component={ContactContainer}
                        />
                    </Switch>
                </div>
            </Router>
        </div>
    );
  }
}

export default App;
